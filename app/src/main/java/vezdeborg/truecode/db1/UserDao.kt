package vezdeborg.truecode.db1

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    //@Query("SELECT * FROM user")
    @Query("SELECT * FROM user WHERE firstName LIKE :name")
    //@Query("SELECT * FROM user LIMIT 5")
    //@Query("SELECT * FROM user WHERE age < 5") и при добавлении отображаться не будут, попробуй, забавно)
    //fun getAll(): Flow<List<User>>
    fun getAll(name: String): Flow<List<User>>

    //@Insert(onConflict = OnConflictStrategy.REPLACE)
    @Insert(entity = User::class)
    suspend fun insert(user: NewUser)

    @Delete
    suspend fun delete(user: User)

    @Update
    suspend fun update(user: User)
}